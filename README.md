# README #

### What is this repository for? ###

* Technical interview project

### How do I get set up? ###

* Create new python environment
```
conda create --name tech-interview python=3.7
```
* Activate new python environment
```
conda activate tech-interview
```
* Install dependencies
```
pip install -r requirements.txt
```

### What are the interview project tasks? ###

Given an input array of integers, write a function to sort all the odd numbers from the array in descending order in their relative positions. Write unit tests to define the acceptance criteria and confirm the correct execution of the function (see test.py for a sample test).

Requirements:
* Function should return a new array
* Odd numbers should be sorted in descending order
* Positions of even elements must not be affected

### How do I run the tests? ###

```
python test.py
```
