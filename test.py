import pytest
from reverse import reverse_slicer


# Sample test
def test_reverse__with_a_valid_string__should_reverse_the_string():
    # Arrange
    test_string = 'abcde'
    
    # Act
    result = reverse_slicer(test_string)
    
    # Assert
    assert result == 'edcba'
    

# TODO - Create test criteria for odd number sort function
def test_sort_odd_numbers_desc__with_input__should_create_output():
    assert 1 == 2


if __name__ == '__main__':
    pytest.main([__file__])
    